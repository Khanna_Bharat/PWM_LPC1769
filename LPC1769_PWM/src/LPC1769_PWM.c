/*
===============================================================================
 Name        : LPC1769_PWM.c
 Author      : Bharat Khanna
 Version     : V.0
 Copyright   : $(copyright)
 Description : Device driver for PWM on LPC1769
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif
#include <cr_section_macros.h>
#include <stdio.h>
#include <stdlib.h>


#define INTERNAL_CLOCK		    		(4  * 1000 * 1000UL)
#define EXTERNAL_CLOCK          		(12 * 1000 * 1000UL)
#define RTC_CLOCK               		(32768UL)
#define configTICK_RATE_HZ			            ( 1000 )
#define OS_MS(x)						( x / MS_PER_TICK() )        // Ticks to millisecond conversion
#define OS_MS(x)						( x / MS_PER_TICK() )        // Ticks to millisecond conversion


unsigned int sys_get_cpu_clock()
{
	unsigned clock = 0;

	/* Determine clock frequency according to clock register values             */
	if (((LPC_SC->PLL0STAT >> 24) & 3) == 3)
	{ /* If PLL0 enabled and connected */
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = (INTERNAL_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;

	        case 1: /* Main oscillator => PLL0       */
	            clock = (EXTERNAL_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;

	        case 2: /* RTC oscillator => PLL0        */
	            clock = (RTC_CLOCK
	                    * ((2 * ((LPC_SC->PLL0STAT & 0x7FFF) + 1)))
	                    / (((LPC_SC->PLL0STAT >> 16) & 0xFF) + 1)
	                    / ((LPC_SC->CCLKCFG & 0xFF) + 1));
	            break;
	    }
	}
	else
	{
	    switch (LPC_SC->CLKSRCSEL & 0x03)
	    {
	        case 0: /* Int. RC oscillator => PLL0    */
	        case 3: /* Reserved, default to Int. RC  */
	            clock = INTERNAL_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	        case 1: /* Main oscillator => PLL0       */
	            clock = EXTERNAL_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	        case 2: /* RTC oscillator => PLL0        */
	            clock = RTC_CLOCK / ((LPC_SC->CCLKCFG & 0xFF) + 1);
	            break;
	    }
	}

	return clock;
}


void PWM_Init(unsigned int frequencyHz, float percent)
{
	LPC_SC->PCONP |= (1 << 6);
	LPC_SC->PCLKSEL0 &=~ (3<<12);
	LPC_SC->PCLKSEL0 |= (1<<12);
	//LPC_SC->PCLKSEL0 |= (3<<12);

	LPC_PWM1->MCR |= (1 << 1);
	LPC_PWM1->MR0 = (sys_get_cpu_clock() / frequencyHz);

    LPC_PWM1->TCR = (1 << 0) | (1 << 3); // Enable PWM1
    LPC_PWM1->CTCR &= ~(0xF << 0);


	LPC_PINCON->PINSEL4 &=~ (3<<0);
	LPC_PINCON->PINSEL4 |= (1<<0);

    LPC_PWM1->PCR |= (1 << 9);

    const unsigned int dutyCycle = ( (percent * ( sys_get_cpu_clock() / frequencyHz ) ) / 100 );
    LPC_PWM1->MR1 = dutyCycle;
    LPC_PWM1->LER |= (1 << 1);

}

int main(void)
{
	while(1)
	{
	// parameter PWM_Init(frequency, duty cycle);
		PWM_Init(1000,100);
	}

   return -1;
}
